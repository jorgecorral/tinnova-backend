class VeiculosController < ApplicationController
    skip_before_action :verify_authenticity_token

    def listarVeiculos
        veiculos = Veiculo.all
        render status: :ok, json: veiculos
    end

    def filtrarVeiculos
        veiculos = Veiculo.where("veiculo like ?", "%#{params["q"]}%")
        render status: :ok, json: veiculos
    end

    def buscarVeiculo
        veiculo = Veiculo.find(params["id"])
        render status: :ok, json: veiculo
    end

    def veiculosSemana
        veiculos = Veiculo.where("created_at > ?", 1.week.ago)
        render status: :ok, json: veiculos
    end

    def contagemDecada
        veiculos = Veiculo.all
        decadas = {}
        veiculos.each do |veiculo|
            decada = veiculo.ano - (veiculo.ano%10)
            if decadas[decada]
                decadas[decada] += 1
            else
                decadas[decada] = 1
            end
        end
        render status: :ok, json: decadas
    end
    
    def contagemMarcas
        veiculos = Veiculo.group(:marca).count
        render status: :ok, json: veiculos
    end

    def contagemNaoVendidos
        veiculos = Veiculo.where({:vendido => false})
        render status: :ok, json: veiculos
    end

    def adicionarVeiculo
        params.permit!
        veiculo = Veiculo.new({:veiculo => params["veiculo"], :marca => params["marca"], :ano => params["ano"], :descricao => params["descricao"], :vendido => params["vendido"]})
        if veiculo.save 
            render status: :ok, json: veiculo
        else
            render status: :bad_request, json: { errors: veiculo.errors.full_messages }
        end
    end

    def atualizarVeiculo
        veiculo = Veiculo.find(params["id"])
        if veiculo.update({:veiculo => params["veiculo"], :marca => params["marca"], :ano => params["ano"], :descricao => params["descricao"], :vendido => params["vendido"]})
            render status: :ok, json: veiculo
        else
            render status: :bad_request, json: { errors: veiculo.errors.full_messages }
        end
    end

    def alterarStatusVeiculo
        veiculo = Veiculo.find(params["id"])
        if veiculo.update({:vendido => params["vendido"]})
            render status: :ok, json: veiculo
        else
            render status: :bad_request, json: { errors: veiculo.errors.full_messages }
        end
    end

    def deletarVeiculo
        veiculo = Veiculo.find(params["id"])
        veiculo.destroy
        render status: :ok, json: true
    end
end
