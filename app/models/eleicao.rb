class Eleicao
  #Método construtor do objeto. Exemplo de criação de um objeto: Eleicao.new(1000,800,150,50)
  def initialize(totalEleitores, votosValidos, votosBrancos, votosNulos)
    @totalEleitores = totalEleitores.to_f
    @votosValidos = votosValidos.to_f
    @votosBrancos = votosBrancos.to_f
    @votosNulos = votosNulos.to_f
  end

  #Método que retorna o percentual de votos válidos
  def percentualValidos
    percentual = (@votosValidos / @totalEleitores)*100
    return percentual
  end

  #Método que retorna o percentual de votos brancos
  def percentualBrancos
    percentual = (@votosBrancos / @totalEleitores)*100
    return percentual
  end

  #Método que retorna o percentual de votos nulos
  def percentualNulos
    percentual = (@votosNulos / @totalEleitores)*100
    return percentual
  end
end
