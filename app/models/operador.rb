class Operador


  #Classe de operador - Realiza operações matemáticas com números
  #Exemplo de utilização
  #Cria um operador: 
  #op = Operador.new()

  #Método que calcula e retorna o fatorial de um número qualquer
  #Executa a ordenação passando a lista como parametro
  #op.fatorial(6)
  def fatorial(numero)
    #Caso base: 0! = 1
    if numero === 0
      return 1
    else
      #Outros casos executarão em recursividade numero x (numero - 1) até chegar no caso base
      return numero * fatorial(numero-1)
    end
  end

  #Método que retorna a soma de todos os múltiplos de um numero até o limite do ultimoNumero, inclusive.
  #Exemplo de utilização
  #op.somaMultiplos(3,10)
  def somaMultiplos(numero, ultimoNumero)
    ultimoMultiplo = ultimoNumero - (ultimoNumero%numero)
    soma = numero * (ultimoMultiplo/numero) * ((ultimoMultiplo/numero) + 1) / 2
    return soma
  end
  
  #Método que retorna o mmc de dois números (numA e numB)
  #Exemplo de utilização
  #op.mmc(3,5)
  def mmc(numA, numB)
    return numA.lcm(numB)
  end

  #Método que retorna a soma de todos os múltiplos de número A OU de número B até o limite do ultimoNumero, inclusive.
  #Exemplo de utilização
  #op.somaMultiplosDoisNumeros(3,10)
  def somaMultiplosDoisNumeros(numA, numB, ultimoNumero)
    somaMultiplosA = somaMultiplos(numA, ultimoNumero)
    somaMultiplosB = somaMultiplos(numB, ultimoNumero)
    somaMultiplosMMC = somaMultiplos(mmc(numA,numB), ultimoNumero)
    return somaMultiplosA + somaMultiplosB - somaMultiplosMMC
  end

end
