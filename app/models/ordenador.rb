class Ordenador


  #Método que retorna a lista ordenada pelo método de bubbleSort
  #Exemplo de utilização
  #Cria um ordenador: 
  #ord = Ordenador.new()
  #Executa a ordenação passando a lista como parametro
  #ord.bubbleSort([5,3,2,4,7,1,0,6])

  def bubbleSort(lista)
    #Se o tamanho da lista for menor ou igual a 1, retornamos a mesma listagem
    if lista.length <= 1
      return lista
    end
    #Se tiver mais de 2 elementos a função continua criando uma nova listagem que será o retorno ordenado
    novaLista = lista
    #Número de repetições precisam ser o tamanho da lista - 1
    totalRepeticoes = novaLista.length-1
    #Looping que repetirá ordenando os elementos desde a última posição até a primeira
    for repeticao in 0..totalRepeticoes do
      #A cada repetição podemos fixar o último elemento, pois ele estará na posição correta. Então, não precisamos analisá-los nos próximo loopings, por isso o index do último elemento de cada looping sempre diminuirá um.
      ultimoElemento = totalRepeticoes - (repeticao + 1) 
      #Looping para comparação em pares
      for indexElemento in 0..ultimoElemento
        #Se o elemento atual for menor que o próximo, trocamos a posição dos dois
        if novaLista[indexElemento] > novaLista[indexElemento+1]
          novaLista[indexElemento] , novaLista[indexElemento+1] = novaLista[indexElemento+1] , novaLista[indexElemento]
        end
      end
    end
    #Retorna a nova lista ordenada
    return novaLista
  end


end
