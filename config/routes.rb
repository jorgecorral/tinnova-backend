Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get "/veiculos", to: "veiculos#listarVeiculos"
  get "/veiculos/find", to: "veiculos#filtrarVeiculos"
  get "/veiculos/contagemNaoVendido", to: "veiculos#contagemNaoVendidos"
  get "/veiculos/contagemDecada", to: "veiculos#contagemDecada"
  get "/veiculos/contagemMarcas", to: "veiculos#contagemMarcas"
  get "/veiculos/veiculosSemana", to: "veiculos#veiculosSemana"
  get "/veiculos/:id", to: "veiculos#buscarVeiculo"
  post "/veiculos", to: "veiculos#adicionarVeiculo"
  put "/veiculos/:id", to: "veiculos#atualizarVeiculo"
  patch "/veiculos/:id", to: "veiculos#alterarStatusVeiculo"
  delete "/veiculos/:id", to: "veiculos#deletarVeiculo"

end
